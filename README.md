Project Name: Daily Meal Manager
Project Overview:
Creating a Java console application for maintaining weekly breakfast and lunch menu for our company. User can view & update weekly menu. Data will be persisted in database.

Note:
Only JDBC and SQL are used, no ORM framework is used